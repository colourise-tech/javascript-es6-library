// https://mathiasbynens.be/notes/globalthis
function getGlobalThis() {
  // Already exists.
  if (typeof globalThis === 'object' && globalThis != null) {
    return globalThis;
  }

  function getFallbackThis() {
    if (typeof window === 'object' && window != null) {
      return window;
    }
    if (typeof global === 'object' && global != null) {
      return global;
    }
    if (typeof self === 'object' && self != null) {
      return self;
    }
    if (this != null) {
      return this;
    }
    throw new Error('Could not resolve `globalThis`');
  }

  try {
    Object.defineProperty(Object.prototype, '__globalThis__', {
      get: function() {
        return this;
      },
      configurable: true
    });
    if (__globalThis__ == null) {
      // Fallback if the `get` didn't work.
      return getFallbackThis();
    } else {
      return __globalThis__;
    }
  } catch (e) {
    return getFallbackThis();
  } finally {
    delete Object.prototype.__globalThis__;
  }
}
// A polyfill is now just:
//
//  localThis = getGlobalThis();
//  localThis.globalThis = localThis;
//

// correctSpellings(words); // Default.
// correctSpellings(words, { keepOriginals: true }); // Keep the originals.
// correctSpellings(words, { keepOriginals: true, skipWarning: true }); // Keep the originals without warnings.

/*
interface Replacement {
  path?: string;
  original: string;
  corrected: string;
}
*/

/*
interface Options {
  keepOriginals?: boolean;
  skipWarning?: boolean;
}
*/
function correctSpellings(replacementList/*: Array<Replacement> */, options/*?: Options*/) {
  options = options || {};

  var localThis/*: Window */ = getGlobalThis();

  function getStackTrace(skippedCalls) {
    // Skip this function as well.
    skippedCalls = (skippedCalls || 0) + 1;
    var stackTrace;
    try {
      throw new Error();
    } catch (e) {
      stackTrace = e.stack || e['opera#sourceloc'];
    }
    if (stackTrace.substring(0, 6) === 'Error\n') {
      // V8 (at least Chrome) prepends the message on to the stack.  Remove it.
      stackTrace = stackTrace.substring(6);
    }
    // Skip the first N lines.
    while (skippedCalls--) {
      stackTrace = stackTrace.substring(stackTrace.indexOf('\n') + 1);
    }
    return stackTrace;
  }

  function replace(parent, path, original/*: string */, corrected/*: string */) {
    const value = parent[original];
    if (value == null) {
      // The original doesn't exist, don't rename it.
      return;
    }
    Object.defineProperty(parent, corrected, {
      value: value,
    });
    if (options.keepOriginals !== true) {
      // Remove the original.
      Object.defineProperty(parent, original, {
        value: undefined,
      });
    } else if (options.skipWarning !== true) {
      // Deprecate the original.
      var warned = [];
      Object.defineProperty(parent, original, {
        get: function() {
          // Get the stack trace of the place that called this renamed symbol.
          var stackTrace = getStackTrace(1);
          var firstLine = stackTrace.substring(0, stackTrace.indexOf('\n'));
          if (!warned.includes(firstLine)) {
            // Make the deprecation message.
            var message;
            if (path == null) {
              message = '`' + original + '` has been deprecated, please use `' + corrected + '` instead.';
            } else {
              message = '`' + path + '.' + original + '` has been deprecated, please use `' + path + '.' + corrected + '` instead.';
            }
            if (stackTrace.substring(0, 7) === '    at ') {
              // Chrome doesn't print 'Stack trace:',
              console.warn('Warning: ' + message + '\n' + stackTrace);
            } else {
              // Firefox does,
              console.warn('Warning: ' + message + '\nStack trace:\n' + stackTrace);
            }
            // Print a warning with that call site (not here).
            warned.push(firstLine);
          }
          getStackTrace(1);
          return this[corrected];
        },
      });
    }
  }

  // Now we can do the replacements.
  for (var i = 0; i !== replacementList.length; ++i) {
    var replacement = replacementList[i];
    var parent = localThis;
    var path = replacement.path;
    if (path != null) {
      var parts = path.split('.');
      for (var j = 0; j !== parts.length; ++j) {
        if (parent == null) {
          break;
        }
        parent = parent[parts[j]];
      }
    }
    if (parent != null) {
      // Use the parent, or global object.
      replace(parent, path, replacement.original, replacement.corrected);
    }
  }
}

function correctStandardSpellings(options/*?: Options*/) {
  correctSpellings([
    {
      path: 'String.prototype',
      original: 'normalize',
      corrected: 'normalise',
    }, {
      original: 'FinalizationRegistry',
      corrected: 'FinalisationRegistry',
    }, {
      original: 'Math',
      corrected: 'Maths',
    },
  ], options);
}

correctStandardSpellings({
  keepOriginals: true,
});


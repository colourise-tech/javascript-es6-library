// JS doesn't (didn't...) have negative look-behind.  Fortunately we can replicate it:
//
//   (?<!unsigned )int
//
// Becomes:
//
//   ((?!unsigned ).{9}|^.{0,8})int
//
// Where `9` is the length of the negative test, and `0,8` is anything shorter (doesn't apply to
// us, since we're only testing single characters).  Unfortunately this captures.
var DEFAULT_REPLACEMENTS = [
  { from: /\bcolor/g, to: 'colour' },
  { from: /\bColor/g, to: 'Colour' },
  { from: /\bCOLOR/g, to: 'COLOUR' },
  // Need to not match `size`.
  { from: /ize/g, to: 'ise' },
  { from: /IZE/g, to: 'ISE' },
];

var localThis = getGlobalThis();

function correctStandardSpellings(options/*?: Options*/) {
  correctSpelling('String.prototype.normalize', 'normalise', options);
  correctSpelling('FinalizationRegistry', 'FinalisationRegistry', options);
  correctSpelling('Math', 'Maths', options);
}

/*

Call possibilities:

// Rename everything in the object with standard replacements.
correctSpelling(String.prototype);

// Rename everything in the object with standard replacements, recursively.
correctSpelling(String.prototype, true);

// Rename everything in the object with specified replacements.
correctSpelling(String.prototype, [{ from: 'color', to: 'colour' }]);

// Rename everything in the object with specified replacements, recursively.
correctSpelling(String.prototype, [{ from: 'color', to: 'colour' }], true);

// Rename the specified property.
correctSpelling('String.prototype.normalize', 'normalise');

// Rename the specified property, with a module name hint.
correctSpelling('node-string-lib', 'String.prototype.normalize', 'normalise');

// Rename everything in the object with specified replacements, recursively with a module name hint.
correctSpelling('node-string-lib', String.prototype, [{ from: 'color', to: 'colour' }], true);

// Rename the specified property.
correctSpelling(String.prototype, 'normalize', 'normalise');

// Rename the specified property, with a module name hint.
correctSpelling('node-string-lib', String.prototype, 'normalize', 'normalise');

// The last argument can just be a whole object of things:
correctSpelling(String.prototype, {
  recurse: true,
  module: 'node-string-lib',
});

`original` and `from` are equivalent.
`corrected` and `to` are equivalent.
`{ from: 'color', to: 'colour' }` can also be simply `['color', 'colour']`.

Originals and replacements are passed to `String.prototype.replace`, so can be regexp and functions.
The regexp has been extended with `\l`, which is a logical word boundary - i.e. one that programmers
would recognise as such, but written English wouldn't.  For example `correctSpelling` is two words,
but there's no word boundary according to `\b`.  However `/.\l./` would match `tS` in that string.
Just like with `.replace` the search parameter is either a literal string or a RegExp, a given
string is not passed to `new RegExp()`.

*/

/*
interface ReplacementFT<K, T> {
  from: K;
  to: T;
}

interface ReplacementOC<K, T> {
  original: K;
  corrected: T;
}

type Replacement = [string, string] | ReplacementFT<string, string> | ReplacementOC<string, string>;

interface RenameOptions {
  keepOriginal?: boolean;
  skipWarning?: boolean;
}

interface ModuleOptions {
  module?: string;
}

interface ReplacementOptions {
  replacements?: Replacement[];
}

interface RecurseOptions {
  recurse?: boolean;
}


Call possibilities:

// Explicit parent object to replace in.

// Rename everything in the object with standard replacements.
function correctSpelling<T extends object>(parent: T): void;

// Rename everything in the object with standard replacements, recursively.
function correctSpelling<T extends object>(parent: T, recurse: boolean): void;

// Rename everything in the object with standard replacements, with explicit options.
function correctSpelling<T extends object>(parent: T, options: ModuleOptions & RenameOptions & RecurseOptions & ReplacementOptions): void;

// Rename everything in the object with provided replacements.
function correctSpelling<T extends object>(parent: T, replacements: Replacement[]): void;

// Rename everything in the object with provided replacements, recursively.
function correctSpelling<T extends object>(parent: T, replacements: Replacement[], recurse: boolean): void;

// Rename everything in the object with provided replacements, with explicit options.
function correctSpelling<T extends object>(parent: T, replacements: Replacement[], options: ModuleOptions & RenameOptions & RecurseOptions): void;

// Rename one field in the object.
function correctSpelling<T extends object, K extends keyof T, L extends string>(parent: T, from: K, to: L): parent is T & { [to]: typeof T[K] };

// Rename one field in the object, with explicit options.
function correctSpelling<T extends object, K extends keyof T, L extends string>(parent: T, from: K, to: L, options: ModuleOptions & RenameOptions): parent is T & { [to]: typeof T[K] };

// Like above, but with a module hint first.
function correctSpelling<T extends object>(module: string, parent: T): void;
function correctSpelling<T extends object>(module: string, parent: T, recurse: boolean): void;
function correctSpelling<T extends object>(module: string, parent: T, options: RenameOptions & RecurseOptions & ReplacementOptions): void;
function correctSpelling<T extends object>(module: string, parent: T, replacements: Replacement[]): void;
function correctSpelling<T extends object>(module: string, parent: T, replacements: Replacement[], recurse: boolean): void;
function correctSpelling<T extends object>(module: string, parent: T, replacements: Replacement[], options: RenameOptions & RecurseOptions): void;
function correctSpelling<T extends object, K extends keyof T, L extends string>(module: string, parent: T, from: K, to: L): parent is T & { [to]: typeof T[K] };
function correctSpelling<T extends object, K extends keyof T, L extends string>(module: string, parent: T, from: K, to: L, options: RenameOptions): parent is T & { [to]: typeof T[K] };

// Like above, but with a path in to the parent object.
function correctSpelling<T extends object>(parent: T, path: string): void;
function correctSpelling<T extends object>(parent: T, path: string, recurse: boolean): void;
function correctSpelling<T extends object>(parent: T, path: string, options: ModuleOptions & RenameOptions & RecurseOptions & ReplacementOptions): void;
function correctSpelling<T extends object>(parent: T, path: string, replacements: Replacement[]): void;
function correctSpelling<T extends object>(parent: T, path: string, replacements: Replacement[], recurse: boolean): void;
function correctSpelling<T extends object>(parent: T, path: string, replacements: Replacement[], options: ModuleOptions & RenameOptions & RecurseOptions): void;
function correctSpelling<T extends object, K extends keyof T, L extends string>(parent: T, path: string, from: K, to: L): parent is T & { [to]: typeof T[K] };
function correctSpelling<T extends object, K extends keyof T, L extends string>(parent: T, path: string, from: K, to: L, options: ModuleOptions & RenameOptions): parent is T & { [to]: typeof T[K] };

function correctSpelling<T extends object>(module: string, parent: T, path: string): void;
function correctSpelling<T extends object>(module: string, parent: T, path: string, recurse: boolean): void;
function correctSpelling<T extends object>(module: string, parent: T, path: string, options: RenameOptions & RecurseOptions & ReplacementOptions): void;
function correctSpelling<T extends object>(module: string, parent: T, path: string, replacements: Replacement[]): void;
function correctSpelling<T extends object>(module: string, parent: T, path: string, replacements: Replacement[], recurse: boolean): void;
function correctSpelling<T extends object>(module: string, parent: T, path: string, replacements: Replacement[], options: RenameOptions & RecurseOptions): void;
function correctSpelling<T extends object, K extends keyof T, L extends string>(module: string, parent: T, path: string, from: K, to: L): parent is T & { [to]: typeof T[K] };
function correctSpelling<T extends object, K extends keyof T, L extends string>(module: string, parent: T, path: string, from: K, to: L, options: RenameOptions): parent is T & { [to]: typeof T[K] };

// Path and replacement
function correctSpelling(path: string | string[]);
function correctSpelling(path: string[], options: ModuleOptions & RenameOptions & RecurseOptions & ReplacementOptions);
// This one is impossible to determine, because it clashes with `function correctSpelling<T extends object>(module: string, parent: T): void;`
//function correctSpelling(path: string, options: ModuleOptions & RenameOptions & RecurseOptions & ReplacementOptions);
function correctSpelling(path: string | string[], corrected: string);
function correctSpelling(module: string, path: string | string[], corrected: string);
function correctSpelling(path: string | string[], corrected: string, options: ModuleOptions & RenameOptions);
function correctSpelling(module: string, path: string | string[], corrected: string, options: RenameOptions);

// Everything.
function correctSpelling();

*/

function correctSpelling() {
  var module;
  var parent;
  var offset;
  var pathParts;

  // First handle the relatively special cases where only paths are operating on `globalThis`.
  switch (typeof arguments[0]) {
  case 'string':
    switch (typeof arguments[1]) {
    case 'string':
      switch (typeof arguments[2]) {
      case 'string':
        switch (typeof arguments[3]) {
        case 'object':
          // function correctSpelling(module: string, path: string, corrected: string, options: RenameOptions);
          return correctSingleSpelling(localThis, arguments[1].split('.'), arguments[2], Object.assign({ module: arguments[0] }, arguments[3]));
        case 'undefined':
          // function correctSpelling(module: string, path: string, corrected: string);
          return correctSingleSpelling(localThis, arguments[1].split('.'), arguments[2], { module: arguments[0] });
        default:
          return false;
        }
      case 'object':
        // function correctSpelling(path: string, corrected: string, options: ModuleOptions & RenameOptions);
        return correctSingleSpelling(localThis, arguments[0].split('.'), arguments[1], arguments[2]);
      case 'undefined':
        // function correctSpelling(path: string, corrected: string);
        return correctSingleSpelling(localThis, arguments[0].split('.'), arguments[1], {});
      default:
        return false;
      }
    case 'object':
      if (arguments[1] instanceof Array) {
        // Path component array.
        if (typeof arguments[2] !== 'string') {
          return false;
        }
        switch (typeof arguments[3]) {
        case 'object':
          // function correctSpelling(module: string, path: string[], corrected: string, options: RenameOptions);
          return correctSingleSpelling(localThis, arguments[1], arguments[2], Object.assign({ module: arguments[0] }, arguments[3]));
        case 'undefined':
          // function correctSpelling(module: string, path: string[], corrected: string);
          return correctSingleSpelling(localThis, arguments[1], arguments[2], { module: arguments[0] });
        default:
          return false;
        }
      }
      // The object comes after the module.
      module = arguments[0];
      parent = arguments[1];
      offset = 2;
      break;
    case 'undefined':
      return correctMultipleSpellings(localThis, arguments[0].split('.'), {});
    default:
      // ???
      return false;
    }
    break;
  case 'object':
    if (arguments[0] instanceof Array) {
      // Path component array.
      switch (typeof arguments[1]) {
      case 'object':
        // function correctSpelling(path: string[], options: ModuleOptions & RenameOptions & RecurseOptions & ReplacementOptions);
        return correctMultipleSpellings(localThis, arguments[0], arguments[1]);
      case 'string':
        break;
      default:
        return false;
      }
      switch (typeof arguments[2]) {
      case 'object':
        // function correctSpelling(path: string[], corrected: string, options: ModuleOptions & RenameOptions);
        return correctSingleSpelling(localThis, arguments[0], arguments[1], arguments[2]);
      case 'undefined':
        // function correctSpelling(path: string[], corrected: string);
        return correctSingleSpelling(localThis, arguments[0], arguments[1], {});
      default:
        return false;
      }
    }
    // Explicit object given.
    parent = arguments[0];
    offset = 1;
    break;
  case 'undefined':
    // Just do everything everywhere!
    offset = 0;
    break;
  default:
    // ???
    return false;
  }

  // Get deep nesting within the object.  Find it.
  if (typeof arguments[offset] === 'string') {
    pathParts = arguments[offset].split('.');
    ++offset;
  }

  if (parent == null) {
    // To cover the corner-case where:
    //
    //   function correctSpelling(path: string, options: ModuleOptions & RenameOptions & RecurseOptions & ReplacementOptions);
    //
    // Can't be used.  Instead, use:
    //
    //   function correctSpelling<T extends object>(parent: T, path: string, options: ModuleOptions & RenameOptions & RecurseOptions & ReplacementOptions): void;
    //
    // With `parent = null`.
    parent = localThis;
  }

  // Now we know we have an explicit object, we can start decoding the remaining arguments.  The
  // only difference between many of the function types is the `module` parameter being first or
  // not, and `offset` takes care of that.
  switch (typeof arguments[offset]) {
  case 'undefined':
    // function correctSpelling<T extends object>(parent: T): void;
    // function correctSpelling<T extends object>(module: string, parent: T): void;
    return correctMultipleSpellings(parent, pathParts || [], { module: module });
  case 'boolean':
    // function correctSpelling<T extends object>(parent: T, recurse: boolean): void;
    // function correctSpelling<T extends object>(module: string, parent: T, recurse: boolean): void;
    return correctMultipleSpellings(parent, pathParts || [], { module: module, recurse: arguments[offset] });
  case 'object':
    if (arguments[offset] instanceof Array) {
      switch (typeof arguments[offset + 1]) {
      case 'undefined':
        // function correctSpelling<T extends object>(parent: T, replacements: Replacement[]): void;
        // function correctSpelling<T extends object>(module: string, parent: T, replacements: Replacement[]): void;
        return correctMultipleSpellings(parent, pathParts || [], { module: module, replacements: arguments[offset] });
      case 'boolean':
        // function correctSpelling<T extends object>(parent: T, replacements: Replacement[], recurse: boolean): void;
        // function correctSpelling<T extends object>(module: string, parent: T, replacements: Replacement[], recurse: boolean): void;
        return correctMultipleSpellings(parent, pathParts || [], { module: module, replacements: arguments[offset], recurse: arguments[offset + 1] });
      case 'object':
        // function correctSpelling<T extends object>(parent: T, replacements: Replacement[], options: RenameOptions & RecurseOptions): void;
        // function correctSpelling<T extends object>(module: string, parent: T, replacements: Replacement[], options: RenameOptions & RecurseOptions): void;
        return correctMultipleSpellings(parent, pathParts || [], Object.assign({ module: module, replacements: arguments[offset] }, arguments[offset + 1]));
      default:
        // ???
        return false;
      }
    }
    // function correctSpelling<T extends object>(parent: T, options: RenameOptions & DeepOptions): void;
    // function correctSpelling<T extends object>(module: string, parent: T, options: RenameOptions & DeepOptions): void;
    return correctMultipleSpellings(parent, pathParts || [], Object.assign({ module: module }, arguments[offset]));
  case 'string':
    if (pathParts == null) {
      // Should have had two strings in a row to get here.
      return false;
    }
    switch (typeof arguments[offset + 1]) {
    case 'object':
      // function correctSpelling<T extends object, K extends keyof T, L extends string>(parent: T, from: K, to: L, options: RenameOptions): parent is T & { [to]: typeof T[K] };
      // function correctSpelling<T extends object, K extends keyof T, L extends string>(module: string, parent: T, from: K, to: L, options: RenameOptions): parent is T & { [to]: typeof T[K] };
      return correctSingleSpelling(parent, pathParts, arguments[offset], Object.assign({ module: module }, arguments[offset + 1]));
    case 'undefined':
      // function correctSpelling<T extends object, K extends keyof T, L extends string>(parent: T, from: K, to: L): parent is T & { [to]: typeof T[K] };
      // function correctSpelling<T extends object, K extends keyof T, L extends string>(module: string, parent: T, from: K, to: L): parent is T & { [to]: typeof T[K] };
      return correctSingleSpelling(parent, pathParts, arguments[offset], { module: module });
    default:
      return false;
    }
  default:
    // ???
    return false;
  }
}

function correctMultipleSpellings(parent, pathParts, options) {
  var recurse = options.recurse !== false;
  var replacements = options.replacements || DEFAULT_REPLACEMENTS;
  // Set up the root path.
  var root;
  var length = pathParts.length;
  if (length === 0) {
    root = '';
  } else {
    root = pathParts.join('.') + '.';
    for (var i = 0; i !== length; ++i) {
      parent = parent[pathParts[i]];
      if (parent == null) {
        // The current doesn't exist, don't rename it.
        return;
      }
    }
  }

  length = replacements.length;

  function correctObjectSpellings(that, path, ancestors) {
    if (ancestors.includes(that)) {
      // Found a recursive object.  Back out.
      return;
    }
    // Add this object to the list of ancestors.
    ancestors.push(that);
    var keys = Object.getOwnPropertyNames(that);
    for (var i = 0; i !== keys.length; ++i) {
      var oldKey = keys[i];
      var newKey = oldKey;
      // Apply all the replacements to this key.
      for (var j = 0; j !== length; ++j) {
        var replacement = replacements[j];
        // One of these defines this replacement pair!
        newKey = newKey.replace(replacement.original || replacement.from || replacement[0], replacement.corrected || replacement.to || replacement[1]);
      }
      var value = that[oldKey];
      if (oldKey !== newKey) {
        // It's changed.
        replaceSpelling(that, value, path, oldKey, newKey, options);
      }
      if (recurse && typeof value === 'object' && value != null && !(value instanceof Array)) {
        correctObjectSpellings(value, path + (path === '`' ? '' : '.') + newKey, ancestors);
      }
    }
    // Remove outselves again.
    ancestors.pop();
  }

  correctObjectSpellings(parent, '`' + root, []);
}

function correctSingleSpelling(value, pathParts, corrected/*: string */, options) {
  var parent;
  var message = '`';
  var original;
  for (var i = 0; i !== pathParts.length; ++i) {
    if (original != null) {
      message = message + original + '.';
    }
    original = pathParts[i];
    parent = value;
    value = parent[original];
    if (value == null) {
      // The original doesn't exist, don't rename it.
      return;
    }
  }

  replaceSpelling(parent, value, message, original, corrected, options)
}

function replaceSpelling(parent, value, message, original, corrected/*: string */, options) {
  // Get the original descriptor to preserve options.
  var descriptor = Object.getOwnPropertyDescriptor(parent, original);

  // Create the new version.
  Object.defineProperty(parent, corrected, {
    value: value,
    writable: !!descriptor.writable,
    enumerable: !!descriptor.enumerable,
    configurable: !!descriptor.configurable,
  });

  if (options.keepOriginals !== true) {
    // Remove the original.
    delete parent[original];
  } else if (options.skipWarning !== true) {
    // Deprecate the original.
    var warned = [];
    // Make the deprecation message.
    message = message + original + '`';
    if (options.module != null) {
      message = message + ' in module "' + options.module + '"';
    }
    message = message + ' has been deprecated, please use `' + corrected + '` instead.';
    Object.defineProperty(parent, original, {
      get: function() {
        // Get the stack trace of the place that called this renamed symbol.
        var stackTrace = getStackTrace(1);
        var firstLine = stackTrace.substring(0, stackTrace.indexOf('\n'));
        if (!warned.includes(firstLine)) {
          if (stackTrace.substring(0, 7) === '    at ') {
            // Chrome doesn't print 'Stack trace:',
            console.warn('Warning: ' + message + '\n' + stackTrace);
          } else {
            // Firefox does,
            console.warn('Warning: ' + message + '\nStack trace:\n' + stackTrace);
          }
          // Print a warning with that call site (not here).
          warned.push(firstLine);
        }
        getStackTrace(1);
        return this[corrected];
      },
    });
  }
}

// https://mathiasbynens.be/notes/globalthis
function getGlobalThis() {
  // Already exists.
  if (typeof globalThis === 'object' && globalThis != null) {
    return globalThis;
  }

  function getFallbackThis() {
    if (typeof window === 'object' && window != null) {
      return window;
    }
    if (typeof global === 'object' && global != null) {
      return global;
    }
    if (typeof self === 'object' && self != null) {
      return self;
    }
    if (this != null) {
      return this;
    }
    throw new Error('Could not resolve `globalThis`');
  }

  try {
    Object.defineProperty(Object.prototype, '__globalThis__', {
      get: function() {
        return this;
      },
      configurable: true
    });
    if (__globalThis__ == null) {
      // Fallback if the `get` didn't work.
      return getFallbackThis();
    } else {
      return __globalThis__;
    }
  } catch (e) {
    return getFallbackThis();
  } finally {
    delete Object.prototype.__globalThis__;
  }
}
// A polyfill is now just:
//
//  localThis = getGlobalThis();
//  localThis.globalThis = localThis;
//

function getStackTrace(skippedCalls) {
  // Skip this function as well.
  skippedCalls = (skippedCalls || 0) + 1;
  var stackTrace;
  try {
    throw new Error();
  } catch (e) {
    stackTrace = e.stack || e['opera#sourceloc'];
  }
  if (stackTrace.substring(0, 6) === 'Error\n') {
    // V8 (at least Chrome) prepends the message on to the stack.  Remove it.
    stackTrace = stackTrace.substring(6);
  }
  // Skip the first N lines.
  while (skippedCalls--) {
    stackTrace = stackTrace.substring(stackTrace.indexOf('\n') + 1);
  }
  return stackTrace;
}

function searchAndReplace(search, replace) {
  // Are these two parameters a valid combination of search and replace parameters?
  switch (typeof search) {
  case 'string':
    // Simple string search.  Fine.
    break;
  case 'object':
    if (search instanceof RegExp) {
      // RegExp search.  Fine.
      break;
    }
    return false;
  default:
    return false;
  }
  switch (typeof replace) {
  case 'string':
    // Simple string replacement.  Fine.
    return true;
  case 'function':
    // Function replacement.  Fine.
    return true;
  }
  return false;
}

